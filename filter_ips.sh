#!/bin/bash

cat servers.log | grep -E '[0-9]\.tcp.+ngrok.+:[0-9]{3,}' | cut -d ' ' -f 2 | sed 's/)//' | sort | uniq > selected-ips.txt
