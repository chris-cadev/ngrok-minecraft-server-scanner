from scanner import check_server
import click
from mcstatus import JavaServer


@click.command()
@click.option('--ip', required=True, help='IP address of the server')
def ping(ip):
    try:
        server = JavaServer.lookup(ip)
        ping = round(server.ping())
        status = server.status()
        players = f'({status.players.online}/{status.players.max})'
        log_format = '(IP: {ip}) (Ping: {ping}ms) (Version: "{version})") (Players: {players})'
        print(log_format.format(
            ip=ip,
            ping=ping,
            version=status.version.name,
            players=players
        ))
    except Exception as e:
        return


if __name__ == '__main__':
    ping()
