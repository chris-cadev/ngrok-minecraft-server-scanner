#!/usr/bin/env python3

from concurrent.futures import ThreadPoolExecutor
import os
from mcstatus import JavaServer
import random


def log(text):
    with open(logpath, 'a') as f:
        f.write(text+"\n")
        f.close()


def check_server(ip):
    try:
        server = JavaServer.lookup(ip)
        ping = round(server.ping())
        status = server.status()
    # print error message
    except Exception as e:
        log(f'(IP: {ip}) (Error: {e})')
        return
    vers = status.version.name
    count = status.players.online
    sample = status.players.sample
    if sample == None:
        sample = []
    names = [i.name for i in sample]
    players = f'{", ".join(names[:3])} ({count}/{status.players.max})'
    log(f'(IP: {ip}) (Ping: {ping}ms) (Version:'f' "{vers}") (Players: {players})')


def ngrok_ip_list():
    ip_format = '{subdomain}.tcp{country}.ngrok.io:{port}'
    countries = ['']  # , '.au', '.eu', '.ru']
    ip_list = []
    for country in countries:
        for server_number in range(3):
            for port in range(10000, 40000):
                ip_list.append(ip_format.format(
                    subdomain=server_number, country=country, port=port))

    return ip_list


if __name__ == '__main__':
    logpath = "servers.log"
    selected_ips_file = 'selected-ips.txt'

    if not os.path.exists(logpath):
        open(logpath, 'w').close()
    if not os.path.exists(selected_ips_file):
        open(selected_ips_file, 'w').close()

    pool = ThreadPoolExecutor(max_workers=20)

    ip_list = ngrok_ip_list()
    tryed_ips = []
    selected_ips = []
    with open(selected_ips_file, 'r') as f:
        selected_ips = f.read().splitlines()

    while len(tryed_ips) < len(ip_list):
        ip = ip_list[int(len(ip_list)*random.random())]
        if ip in tryed_ips:
            continue
        if ip in selected_ips:
            continue
        pool.submit(check_server, ip)
        selected_ips.append(ip)
        tryed_ips.append(ip)
